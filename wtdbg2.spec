Name:           wtdbg2
Version:        2.5
Release:        1
License:        GPL-3.0-only
Group:          Development/Libraries
Summary:        Third-generation gene sequencing assembly software
URL:            http://www.wtdbg2.org
Source0:        %{name}-%{version}.tar.gz
Source1:        sse2neon.h
BuildRequires:  wget git zlib-devel
Patch100:       support_aarch64_platform.patch

%description
Wtdbg2 is a third-generation sequencing data (applicable to both pacbio and nanopore) 
denovo assembly software. It is an open source software developed based on the C language

%prep
%setup -n %{name}-%{version}
cp %{SOURCE1} .

%ifarch aarch64
%patch100 -p1
%endif

%build
%make_build

%install
mkdir -p %{buildroot}/usr/bin
cp -fvu kbm2 wtdbg2 wtdbg-cns wtpoa-cns pgzf %{buildroot}/usr/bin

%files
%doc README.md README-ori.md
%license LICENSE.txt
/usr/bin/*


%changelog
* Sat Feb 20 2021 zhangshaoning <zhangshaoning@uniontech.com> - 2.5-1
- Package init

